%global _empty_manifest_terminate_build 0
Name:		python-marshmallow
Version:	3.20.1
Release:	1
Summary:	A lightweight library for converting complex datatypes to and from native Python datatypes.
License:	MIT
URL:		https://github.com/marshmallow-code/marshmallow
Source0:	https://files.pythonhosted.org/packages/e4/e0/3e49c0f91f3e8954806c1076f4eae2c95a9d3ed2546f267c683b877d327b/marshmallow-3.20.1.tar.gz
BuildArch:	noarch

Requires:	python3-pytest
Requires:	python3-pytz
Requires:	python3-simplejson
Requires:	python3-tox
Requires:	python3-sphinx
Requires:	python3-sphinx-issues
Requires:	python3-alabaster
Requires:	python3-sphinx-version-warning
Requires:	python3-autodocsumm
Requires:	python3-mypy
Requires:	python3-flake8
Requires:	python3-flake8-bugbear
Requires:	python3-pre-commit

%description
marshmallow is an ORM/ ODM/ framework-agnostic library for converting complex datatypes, such as objects, to and from native Python datatypes.

%package -n python3-marshmallow
Summary:	A lightweight library for converting complex datatypes to and from native Python datatypes.
Provides:	python-marshmallow = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools

%description -n python3-marshmallow
marshmallow is an ORM/ ODM/ framework-agnostic library for converting complex datatypes, such as objects, to and from native Python datatypes.

%package help
Summary:	Development documents and examples for marshmallow
Provides:	python3-marshmallow-doc

%description help
marshmallow is an ORM/ ODM/ framework-agnostic library for converting complex datatypes, such as objects, to and from native Python datatypes.

%prep
%autosetup -n marshmallow-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-marshmallow -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Aug 01 2023 niuyaru <niuyaru@kylinos.cn> - 3.20.1-1
- Update package to version 3.20.1

* Mon Nov 21 2022 wangjunqi <wangjunqi@kylinos.cn> - 3.19.0-1
- Update package to version 3.19.0

* Wed Jun 29 2022 yaoxin <yaoxin30@h-partners.com> - 3.17.0-1
- Update to 3.17.0

* Wed Aug 11 2021 Python_Bot <Python_Bot@openeuler.org> - 3.13.0-1
- Upgrade marshmallow to 3.13.0-1

* Thu Aug 13 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
